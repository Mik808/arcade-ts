import { useContext, useMemo } from "react";
import { FeedContext } from "../../context/feed-context";
import { Link } from "react-router-dom";

import styles from "./gameSuggestions.module.css";

export default function GameSuggestions(props: any) {
    const data: any = useContext(FeedContext);
    console.log("data", data);

    let generateRandom = (total: number, maxValue: number) => {
        var randomArr: any = [];
        while (randomArr.length < total) {
            var randomNum = Math.floor(Math.random() * maxValue);
            if (!(randomArr.find((el: number) => el === randomNum))) {
                randomArr.push(randomNum);
            }
        }
        // debugger;
        console.log("randomArr", randomArr);
        return randomArr;
    }

    const memoizedValue = useMemo(() => generateRandom(4, data.length), [4, data.legnth]);

    return (
        <div className={styles.gameSuggestions}>
            <h2>You may also like</h2>
            {memoizedValue.map((num: number, i: number) => {
                return <Link key={i} to={data[num].id} className={styles.link}>
                    <img src={`${data[num].assetOriginUrl}${data[num].squareIcon152}`} alt="img" className={styles.img} />
                </Link>
            })}
        </div>
    )
}