export const loadState = (storeName:any) => {
    try {
        const serializedState = localStorage.getItem(storeName);
        if (serializedState === null) {
            return undefined;
        }
        console.log("loadState", JSON.parse(serializedState));
        return JSON.parse(serializedState);
    } catch (err) {
        return undefined;
    }
};

export const saveState = (storeName:any, store:any) => {
    try {
        const serializedState = JSON.stringify(store);
        localStorage.setItem(storeName, serializedState);
        console.log("saveState", serializedState);
    } catch (err) {
        // ignore write erorrs
    }
};