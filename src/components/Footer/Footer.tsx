import React from "react";
import { Link } from "react-router-dom"

import styles from "./footer.module.css";

export default function Footer(props:any) {

    // get the current categories from the feed
    let getCategories = () => {
        var categories:any = [];
        props.data.forEach((game:any) => {
            game.feedMetadata.categories.forEach((category:any) => {
                if (categories.length === 0) {
                    categories.push(category);
                } else {
                    if ( (categories.findIndex((el:any) => el === category)) == -1 ) { // add if there is no category in the array
                        categories.push(category);
                    }
                }
            })
        })
        return categories;
    }

    // let footerCategories = getCategories();
    // console.log("footerCategories", footerCategories);

    // console.log("footer props", props);
    
    return (
        <div className={styles.footer}>
            <div className={styles.top}>
                <div className={`title ${styles.title}`}>ArenaName</div>
                <ul className={styles.list}>
                    {getCategories().map((category:any, i:number) => {
                        return (
                            <li key={i}>
                                <Link to={`/category/${category}`}>{category}</Link> 
                            </li>
                        )
                    })}
                </ul>
            </div>
            <div className={styles.bottom}>
                <div>(c) Arena.com</div>
                <div>2020</div>
            </div>
        </div>
    )
}