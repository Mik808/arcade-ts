import { useContext, useEffect } from "react";
import { FeedContext } from "../../context/feed-context";
import { GameContext, GameContextProvider } from "../../context/game-context"
import { useParams } from "react-router-dom";
import GameSuggestions from "../GameSuggestions/GameSuggestions";
import GameSideTabs from "../GameSideTabs/GameSideTabs";

import styles from "./gamePage.module.css";

export default function GamePage(props: any) {
    // get the clicked game
    let { id }: { id: string } = useParams();
    // console.log("id", id);

    const data: any = useContext(FeedContext); // data is empty after page reload
    console.log("gamepage data", data);
    let game: any = data.find((game: any) => { return game.id === id });
    console.log("game", game);

    const [state, dispatch] = useContext(GameContext);

    useEffect(() => {
        // console.log("dispatch", dispatch);
        dispatch({
            type: "ADD_GAME",
            payload: game
        });
    }, [game]);

    return (
        <div>
            {game ?
                <div>
                    <div className={styles.game}>
                        <img src={`${game.assetOriginUrl}${game.squareIcon192}`} alt="img" className={styles.img} />
                        <div className={styles.nameBlock}>
                            <h2 className={styles.title}>{game.name}</h2>
                            <div className={styles.locales}>{game.locales.replace(/[\[\]"]+/g, '').split(",").map((locale: string, i: number) => {
                                return <div key={i} className={`${styles.locale} ${locale === "en" ? styles.localeActive : ""}`}>{locale}</div>
                            })}</div>
                        </div>
                        <div className={styles.description}>{game.description}</div>
                    </div>
                    <GameSideTabs />
                </div>
                : null}

        </div>

    )
}
