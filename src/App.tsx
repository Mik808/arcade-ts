import React, { useState, useEffect } from "react";
import './App.css';
import Layout from "./components/Layout/Layout";
import Routes from "./Routes";
import { BrowserRouter } from "react-router-dom";
import { FeedContextProvider } from "./context/feed-context";

export const StateContext:any = React.createContext("my_context");

function App() {
    // const [data, setData] = useState([]);

    // useEffect(() => {
    //     fetch("https://raw.githubusercontent.com/fortemka/test_task/master/test-task-feed_en.json")
    //         .then(response => response.json())
    //         .then(data => setData(data));
    // });

    // console.log("data", JSON.stringify(data));

    return (
        <BrowserRouter>
            <div className="App">
                <FeedContextProvider>
                    <Layout>
                        <Routes />
                    </Layout>
                </FeedContextProvider>
            </div>
        </BrowserRouter>
    );
}

export default App;
