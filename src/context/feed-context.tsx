import React, { useState, createContext, useEffect } from "react";

// Create Context Object
export const FeedContext: any = createContext("FeedContext");

// Create a provider for components to consume and subscribe to changes
export const FeedContextProvider = (props: any) => {
    const [data, setData] = useState([]);

    useEffect(() => {
            console.log("useEffect saveState");
            fetch("https://raw.githubusercontent.com/fortemka/test_task/master/test-task-feed_en.json")
                .then(response => response.json())
                .then(feed => { setData(feed); })
        
    }, []);

    return (
        <FeedContext.Provider value={data}>
            {props.children}
        </FeedContext.Provider>
    );
}