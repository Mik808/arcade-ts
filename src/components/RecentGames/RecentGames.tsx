import { GameContextProvider, GameContext } from "../../context/game-context";
import { useContext } from "react";

import styles from "./recentGames.module.css";


export default function RecentGames(props: any) {
    const [recentGames] = useContext(GameContext);
    console.log("recentGames", recentGames);
    let recents: any;
    if (recentGames.games && recentGames.games.length > 0) {
        recents = recentGames.games.filter((el:any) => {return el !== null});
        console.log("recents", recents);
    }

    return (
        <div>
            <div className={styles.title}>RecentGames</div>
            {recents && recents.length > 0 && recents.map((el:any, i:number) => {
                return <div key={i}>{el.name}</div>
            })}
        </div>
    )
}