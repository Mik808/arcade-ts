import React from "react";
import Header from "../Header/Header";
import Footer from "../Footer/Footer"
import { useContext } from "react";
import { FeedContext } from "../../context/feed-context";
import { GameContextProvider } from "../../context/game-context";

export default function Layout(props: any) {
    const data = useContext(FeedContext);

    // console.log("context layout", data);

    return (
        <div>
            <Header />
                <GameContextProvider>
                    {props.children}
                </GameContextProvider>
            <Footer data={data} />
        </div>
    )
}
