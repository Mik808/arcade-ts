import React, { useContext, useReducer } from "react";
import { useParams } from 'react-router-dom';
import { FeedContext } from "../../context/feed-context";
import { Link } from "react-router-dom";

import styles from "./category.module.css";

export default function Category(props:any) {
    const data = useContext(FeedContext);

    let { name }: {name:string} = useParams();

    let filterGames = (games:any, categoryName:any) => {
        if (categoryName) {
            // console.log("categoryName", categoryName)
            var filteredGames:any = [];
            games.forEach((game:any) => {
                game.feedMetadata.categories.forEach((category:string) => {
                    if (category === categoryName) {
                        // console.log("game", game);
                        filteredGames.push(game);
                    }
                })
            });
            // return filteredGames;
            return filteredGames.map((game:any, i:number) => {
                return (
                    <Link to={`/game/${game.id}`}  key={i} className={styles.game}>
                        <img src={`${game.assetOriginUrl}${game.squareIcon192}`} alt="img"/>
                        <div className={styles.gameTitle}>{game.name}</div>
                    </Link>
                )
            })
        }
    }

    return (
        <div className={styles.category}>
            { name && <div className={styles.title}>{` ${name} Games`}</div> } 
            <div className={styles.wrapper}>{filterGames(data, name)}</div>
        </div>
    )
}