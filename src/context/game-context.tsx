import React, { useState, createContext, useReducer, useEffect } from "react";
import { loadState, saveState } from "../utils/persistToLS.js";

const initialState: any = { games: [] };
const LS: any = loadState("recentGames");
// Create Context Object
export const GameContext: any = createContext("gameContext");

// Create a provider for components to consume and subscribe to changes
export const GameContextProvider = (props: any) => {
    const [state, dispatch] = useReducer(reducer, LS || initialState);

    function reducer(state: any, action: any) {
        switch (action.type) {
            case "ADD_GAME":
                console.log("reducer state", state);

                // add to array if the game does not already exist in the array
                let newGame: any;
                if (state.games.length) {
                    // filter null and undefined values in the array
                    const filteredGames: any = state.games.filter((el: any) => { return el !== null && el !== undefined });
                    if (!(filteredGames.filter((e: any) => e.id === action.payload.id).length > 0)) {
                        newGame = action.payload;
                    }

                } else {
                    newGame = action.payload;
                }

                if (newGame) {
                    return { games: [...state.games, newGame] }
                } else {
                    return {games: [...state.games]};
                }

            default:
                throw new Error();
        }
    }

    console.log("state2", state);
    useEffect(() => {
        saveState("recentGames", state);
    }, [state]);

    return (
        <GameContext.Provider value={[state, dispatch]}>
            {props.children}
        </GameContext.Provider>
    );
};