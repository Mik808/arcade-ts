import { Route, Switch } from "react-router-dom";
import GamePage from "./components/GamePage/GamePage";
import GameSearchResults from "./components/Search/GameSearchResults";
import Category from "./components/Categories/Category";

const Routes = () => (
        <Switch>
            <Route path="/" exact component={Category} />
            <Route path="/game/:id" exact component={GamePage} />
            <Route path="/search" exact component={GameSearchResults} />
            <Route path="/category/:name" component={Category} />
        </Switch>
);

export default Routes;